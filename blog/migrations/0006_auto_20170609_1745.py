# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-09 12:15
from __future__ import unicode_literals

import ckeditor_uploader.fields
import datetime
from django.db import migrations, models
from django.utils.timezone import utc
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20170609_1359'),
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateTimeField(default=django.utils.timezone.now)),
                ('published', models.BooleanField(choices=[(True, 'Yes'), (False, 'No')], default=True)),
                ('featured', models.BooleanField(choices=[(True, 'Yes'), (False, 'No')], default=True)),
                ('title', models.CharField(max_length=150)),
                ('author_name', models.CharField(max_length=150)),
                ('intro_text', models.TextField(blank=True, null=True)),
                ('image', models.FileField(blank=True, max_length=200, null=True, upload_to='blogs/intro-images')),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True)),
                ('tags', models.CharField(max_length=250)),
            ],
        ),
        migrations.AlterField(
            model_name='photo',
            name='date_added',
            field=models.DateField(default=datetime.datetime(2017, 6, 9, 12, 15, 6, 912294, tzinfo=utc)),
        ),
    ]
