from django.conf.urls import url
from . import views
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView

urlpatterns = [

    url(r'^$', RedirectView.as_view(url='/1', permanent=False)),
    url(r'^(?P<page>\d+)/$', views.index, name="index"),
    url(r'^blogs/(?P<page>\d+)/$', views.blogs, name="blogs"),
    url(r'^philosophy/', views.philosophy, name="philosophy"),
    url(r'^contact/$', views.contact, name="contact"),
]
