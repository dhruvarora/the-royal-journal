# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Photo
from .models import Blog


class PhotoAdmin(admin.ModelAdmin):
    list_display = ("title", "caption", "image", "date_added", "published", "featured")
    search_fields = ["title"]
    list_editable = ["published", "featured"]
    list_filter = ["published", "featured"]

admin.site.register(Photo, PhotoAdmin)
admin.site.register(Blog)
