# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Photo, Blog
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger




def index(request, page):
    page=int(page)
    photos_list=Photo.objects.all()
    paginator = Paginator(photos_list, 6)

    try:
        photos = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        photos = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        photos = paginator.page(paginator.num_pages)


    return render(request, "index.html", {
        'photos':photos
    })

def blogs(request, page):
    page=int(page)
    blogs_list=Blog.objects.all()
    paginator = Paginator(blogs_list, 6)

    try:
        blogs = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        blogs = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        blogs = paginator.page(paginator.num_pages)


    return render(request, "blogs/blog.html", {
        'blogs':blogs
    })

def philosophy(request):
    return render(request, "philosophy.html", {})

def contact(request):
    return render(request, "contact.html", {})
